const CONSTANTS = {
	PAYMENT_FETCH_BEGIN: "PAYMENT_FETCH_BEGIN",
	PAYMENT_FETCH_SUCCESS: "PAYMENT_FETCH_SUCCESS",
	PAYMENT_FETCH_FAILURE: "PAYMENT_FETCH_FAILURE",
	PAYMENT_ADD_BEGIN: "PAYMENT_ADD_BEGIN",
	PAYMENT_ADD_SUCCESS: "PAYMENT_ADD_SUCCESS",
	PAYMENT_ADD_FAILURE: "PAYMENT_ADD_FAILURE",
	PAYMENT_DELETE_BEGIN: "PAYMENT_DELETE_BEGIN",
	PAYMENT_DELETE_SUCCESS: "PAYMENT_DELETE_SUCCESS",
	PAYMENT_DELETE_FAILURE: "PAYMENT_DELETE_FAILURE",
};
const initialState = {
	paymentDetails: [],
	loading: false,
	isSuccessful: false,
	deleted: null,
};

export const PaymentReducer = (state = initialState, action) => {
	switch (action.type) {
		case CONSTANTS.PAYMENT_FETCH_BEGIN:
			return {
				...state,
				loading: true,
				isSuccessful: false,
				deleted: false,
			};
		case CONSTANTS.PAYMENT_FETCH_FAILURE:
			return { ...state, error: action.data, loading: false };

		case CONSTANTS.PAYMENT_FETCH_SUCCESS:
			return {
				...state,
				paymentDetails: action.data,
				error: null,
				loading: false,
			};
		case CONSTANTS.PAYMENT_ADD_BEGIN:
			return { ...state, addError: null };
		case CONSTANTS.PAYMENT_ADD_SUCCESS:
			return { ...state, isSuccessful: true };
		case CONSTANTS.PAYMENT_ADD_FAILURE:
			return { ...state, message: action.data.message, isSuccessful: false };
		case CONSTANTS.PAYMENT_DELETE_BEGIN:
			return { ...state, addError: null };
		case CONSTANTS.PAYMENT_DELETE_SUCCESS:
			return { ...state, deleted: action.status };
		case CONSTANTS.PAYMENT_DELETE_FAILURE:
			return { ...state, message: action.data.message };
		default:
			return state;
	}
};
