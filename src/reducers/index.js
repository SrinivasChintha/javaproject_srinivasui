import { combineReducers } from "redux";
import { PaymentReducer } from "./paymentreducer";

const reducer = combineReducers({
	payment: PaymentReducer,
});

export default reducer;
