import axios from "axios";
export const fetchPaymentDetails = () => {
	return (dispatch, getState) => {
		dispatch(fetchPaymentDetailsBegin());
		setTimeout(
			() =>
				axios.get("http://localhost:8085/api/v1/payment/all").then(
					(res) => {
						dispatch(fetchPaymentDetailsSuccess(res.data));
					},
					(err) => {
						dispatch(fetchPaymentDetailsFailure(err));
					}
				),
			3000
		);
	};
};

export const addPaymentDetails = (payment) => {
	return (dispatch, getState) => {
		dispatch(addPaymentDetailsBegin());
		return axios
			.post("http://localhost:8085/api/v1/payment/save", payment)
			.then(
				(res) => {
					dispatch(addPaymentDetailsSuccess());
				},
				(err) => {
					dispatch(addPaymentDetailsFailure(err));
					throw err;
				}
			);
	};
};

const fetchPaymentDetailsSuccess = (data) => {
	return {
		type: "PAYMENT_FETCH_SUCCESS",
		data: data,
	};
};

const fetchPaymentDetailsBegin = () => {
	return {
		type: "PAYMENT_FETCH_BEGIN",
	};
};

const fetchPaymentDetailsFailure = (err) => {
	return {
		type: "PAYMENT_FETCH_FAILURE",
		data: {
			message: "Unable to fetch payment details at this moment, Try later...",
		},
	};
};

const addPaymentDetailsBegin = (err) => {
	return {
		type: "PAYMENT_ADD_BEGIN",
	};
};
const addPaymentDetailsSuccess = () => {
	return {
		type: "PAYMENT_ADD_SUCCESS",
	};
};
const addPaymentDetailsFailure = (err) => {
	return {
		type: "PAYMENT_ADD_FAILURE",
		data: { message: "Unable to add payment details, Try later..." },
	};
};

export const deletePaymentById = (id) => {
	console.log(id);
	return (dispatch, getState) => {
		return axios
			.get("http://localhost:8085/api/v1/payment/deletePaymentById/" + id)
			.then(
				(response) => {
					dispatch(deletePaymentDetailsSuccess(response.data));
				},
				(err) => {
					dispatch(deletePaymentDetailsFailure(err));
				}
			);
	};
};

const deletePaymentDetailsFailure = (err) => {
	return {
		type: "PAYMENT_DELETE_FAILURE",
		data: { message: "Unable to delete payment details, Try later..." },
	};
};

const deletePaymentDetailsSuccess = (deleted) => {
	return {
		type: "PAYMENT_DELETE_SUCCESS",
		status: deleted,
	};
};
