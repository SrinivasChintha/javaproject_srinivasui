import axios from "axios";

export const pingService = () => {
	return (dispatch) => {
		dispatch({
			type: "SERVICE_STATUS_CHECK_BEGIN",
			message: "Currently Invoking....",
		});
		axios.get("http://localhost:8085/api/v1/payment/status").then(
			(res) =>
				dispatch({
					type: "SERVICE_STATUS_CHECK_SUCCESS",
					message: res.data,
				}),
			(err) =>
				dispatch({
					type: "SERVICE_STATUS_CHECK_FAILURE",
					message: "Payment service is down at this moment...",
				})
		);
	};
};
