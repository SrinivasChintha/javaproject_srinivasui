import axios from "axios";

const API_URL = " http://localhost:8085/api/v1/payment/";

class PaymentService {
	getStatus = () => {
		return axios
			.get(API_URL + "status")
			.then(function (response) {
				console.log("Authenticated");
				console.log(response.data);
			})
			.catch(function (error) {
				console.log("Error on Authentication");
			});
	};
}
export default new PaymentService();
