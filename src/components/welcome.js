import React from "react";

const Welcome = () => {
	return (
		<div className="starter-template">
			<h1>Welcome to the Payments Bank !!</h1>
			<p className="lead">
				Payments bank is most trusted bank in the world.
				<br /> All your money will be allways safe & secure.
			</p>
		</div>
	);
};
export default Welcome;
