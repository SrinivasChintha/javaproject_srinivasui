import { Switch, Route, Link, BrowserRouter } from "react-router-dom";
import About from "./about";
import Welcome from "./welcome";
import Payments from "./payments";
import AddPayment from "./addpayment";
export const Navbar = () => {
	return (
		<div>
			<BrowserRouter>
				<nav className="navbar navbar-expand  navbar-dark bg-primary">
					<Link className="navbar-brand" to={"/"}>
						Payments Bank
					</Link>
					<div className="navbar-nav mr-auto">
						<li className="nav-item">
							<Link to={"/payments"} className="nav-link">
								Payments
							</Link>
						</li>
						<li className="nav-item">
							<Link to={"/addpayment"} className="nav-link">
								Add Payment
							</Link>
						</li>
					</div>
					<div className="navbar-nav ml-auto">
						<li className="nav-item">
							<Link to={"/about"} className="nav-link text-capitalize">
								About
							</Link>
						</li>
					</div>
				</nav>
				<Switch>
					<Route exact path={["/", "/home"]} component={Welcome} />
					<Route exact path="/payments" component={Payments} />
					<Route exact path="/addpayment" component={AddPayment} />
					<Route exact path="/about" component={About} />
				</Switch>
			</BrowserRouter>
		</div>
	);
};
