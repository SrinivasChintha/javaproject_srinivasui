import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";

import {
	fetchPaymentDetails,
	deletePaymentById,
} from "../actions/paymentaction";
import { Loder } from "./Loder";

const Payments = (props) => {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchPaymentDetails());
		// eslint-disable-next-line
	}, []);

	const handleDelete = (id) => {
		if (id) {
			dispatch(deletePaymentById(id));
		}
	};
	if (props.deleted) {
		window.location.reload(false);
	}

	return (
		<React.Fragment>
			{props.loading ? (
				<Loder />
			) : (
				<div className="container mt-5">
					<table className="table table-striped ">
						<thead>
							<tr className="p-3 mb-2 bg-primary text-white">
								<th scope="col">Payment Id</th>
								<th scope="col">Payment Date</th>
								<th scope="col">Account Type</th>
								<th scope="col">Amount</th>
								<th scope="col">Customer Id</th>
								<th scope="col">Description</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							{props.payments &&
								props.payments.map((payment) => (
									<tr key={payment.id}>
										<th scope="row">{payment.id}</th>
										<td>{payment.paymentDate}</td>
										<td>{payment.type}</td>
										<td>{payment.amount}</td>
										<td>{payment.customerId}</td>
										<td>
											<lable
												data-toggle="tooltip"
												data-placement="top"
												title={payment.description}
											>
												{/* <p class="col-3 text-truncate">{payment.description}</p> */}
												<span
													class="d-inline-block text-truncate text-primary"
													style={{ maxWidth: "100px" }}
												>
													{payment.description}
												</span>
											</lable>
										</td>
										<td>
											<button
												className="btn btn-danger btn-sm rounded-0"
												type="button"
												data-toggle="tooltip"
												data-placement="top"
												title=""
												data-original-title="Delete"
												onClick={() => handleDelete(payment.id)}
											>
												<i className="fa fa-trash"></i>
											</button>
										</td>
									</tr>
								))}
						</tbody>
					</table>
				</div>
			)}
		</React.Fragment>
	);
};
const mapStateToProps = (state) => {
	return {
		payments: state.payment.paymentDetails,
		loading: state.payment.loading,
		deleted: state.payment.deleted,
	};
};

export default connect(mapStateToProps)(Payments);
