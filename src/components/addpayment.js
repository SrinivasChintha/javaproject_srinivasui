import React, { useState } from "react";
import { addPaymentDetails } from "../actions/paymentaction";
import { connect, useDispatch } from "react-redux";
import { Redirect } from "react-router-dom";

const AddPayment = (props) => {
	const dispatch = useDispatch();
	const initialState = {
		id: "",
		paymentDate: "",
		type: "Select Account Type",
		amount: "",
		customerId: "",
		description: "",
	};

	const [payment, setPayment] = useState(initialState);
	const handleInputChange = (event) => {
		const { name, value } = event.target;
		setPayment({ ...payment, [name]: value });
	};

	if (props.isPaymentSuccessful) {
		return <Redirect to="/payments" />;
	}
	return (
		<div className="container mt-5">
			<h3 className="p-3 mb-2 bg-secondary text-white">Add Payment</h3>
			<form
				onSubmit={(event) => {
					event.preventDefault();
					dispatch(addPaymentDetails(payment));
				}}
			>
				<div className="form-group row mt-5">
					<label htmlFor="inputPaymentId" className="col-sm-2 col-form-label">
						Id
					</label>
					<div className="col-sm-3">
						<input
							type="text"
							name="id"
							className="form-control"
							id="inputPaymentId"
							placeholder="Payment Id"
							value={payment.id}
							required
							onChange={handleInputChange}
						/>
					</div>
				</div>
				<div className="form-group row">
					<label htmlFor="inputPaymentDate" className="col-sm-2 col-form-label">
						Date
					</label>
					<div className="col-sm-3">
						<input
							type="text"
							className="form-control"
							name="paymentDate"
							id="inputPaymentDate"
							placeholder="Payment Date(DD-MM-YYYY)"
							value={payment.paymentDate}
							required
							onChange={handleInputChange}
						/>
					</div>
				</div>

				<div className="form-group row">
					<label htmlFor="inputAccountType" className="col-sm-2 col-form-label">
						Account Type
					</label>
					<select
						className="custom-select custom-select-m col-sm-3"
						name="type"
						onChange={handleInputChange}
					>
						<option value="Select Account Type">Select Account Type</option>
						<option value="Savings">Savings</option>
						<option value="Current">Current</option>
					</select>
				</div>
				<div className="form-group row">
					<label htmlFor="inputAmount" className="col-sm-2 col-form-label">
						Amount
					</label>
					<div className="col-sm-3">
						<input
							type="text"
							name="amount"
							className="form-control"
							id="inputAmount"
							placeholder="Payment Amount"
							value={payment.amount}
							required
							onChange={handleInputChange}
						/>
					</div>
				</div>
				<div className="form-group row">
					<label htmlFor="inputId" className="col-sm-2 col-form-label">
						Customer Id
					</label>
					<div className="col-sm-3">
						<input
							type="text"
							className="form-control"
							name="customerId"
							id="inputId"
							placeholder="Customer Number"
							value={payment.customerId}
							required
							onChange={handleInputChange}
						/>
					</div>
				</div>
				<div className="form-group row">
					<label htmlFor="inputDescription" className="col-sm-2 col-form-label">
						Description
					</label>
					<div className="col-sm-5">
						<input
							type="text"
							className="form-control"
							name="description"
							id="inputDescription"
							placeholder="Please enter payment description"
							value={payment.description}
							required
							onChange={handleInputChange}
						/>
					</div>
				</div>

				<div className="form-group row ">
					<div className="col-sm-6">
						<button type="submit" className="btn btn-primary">
							Add Payment
						</button>
					</div>
				</div>
				{!props.isPaymentSuccessful && props.message && (
					<div className="alert alert-warning m-5" role="alert">
						{props.message}
					</div>
				)}
			</form>
		</div>
	);
};
const mapStateToProps = (state) => {
	return {
		isPaymentSuccessful: state.payment.isSuccessful,
		message: state.payment.message,
	};
};
export default connect(mapStateToProps)(AddPayment);
