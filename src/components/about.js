import React from "react";
import { pingService } from "../actions/paymentstatusaction";

class About extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		const style = this.state.status
			? "alert alert-primary m-5"
			: "alert alert-danger m-5";
		return (
			<div className={style} role="alert">
				<h2>Payment Rest API</h2>
				<p> Payment rest service serves the payment information</p>
				<b>Service Status is: </b>
				{this.state.message}
			</div>
		);
	}

	async componentDidMount() {
		await this.getPaymentStatus();
	}

	async getPaymentStatus() {
		var url = "http://localhost:8085/api/v1/payment/status";
		let result;
		let status;
		try {
			let response = await fetch(url);
			result = await response.text();
			console.log("The data is: " + result);
			status = true;
		} catch (e) {
			console.log("The err is: " + e.toString());
			result = "Payment service is down at this moment...";
			status = false;
		}

		this.setState({ message: result, status: status });
	}
}
export default About;
