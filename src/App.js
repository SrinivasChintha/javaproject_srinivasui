import "./App.css";
import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Navbar } from "./components/navbar";

const App = (props) => (
	<React.Fragment>
		<div className="App">
			<Navbar></Navbar>
		</div>
	</React.Fragment>
);

export default App;
